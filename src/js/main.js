// JAVASCRIPT

//animation for main content services
let boxes = document.querySelectorAll('#mainContent .list div');

boxes.forEach(item => item.addEventListener('mouseover', function(e){
	this.classList.add('animated');
	this.classList.add('pulse');
}));
boxes.forEach(item => item.addEventListener('mouseout', function(e){
	this.classList.remove('animated');
	this.classList.remove('pulse');
}));


// JQUERY

//animation for images inside main content services boxes
let images = $('#mainContent .list div img');

$(images).each(function(i,item){
	$(this).on('mouseover', function(){
		$(this).css('transform','rotate(360deg)');
	});
	$(this).on('mouseout', function(){
		$(this).css('transform','none');
	});
});


