const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const webserver = require('gulp-webserver');


// optimize images 
gulp.task('imageMin', () =>
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('src/img'))
);

// compile Sass with autoprefixer
gulp.task('sass', function(){

	gulp.src('src/sass/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('src/css'));
});
// Webserver
gulp.task('webserver', function(){
	return gulp.src('./')
		.pipe(webserver({
			port: '4000',
			livereload: true,
			open: true
		}));
});
//default task
gulp.task('default', ['imageMin','sass','webserver']);
//watch
gulp.task('watch', function(){
	gulp.watch('src/images/*', ['imageMin']);
	gulp.watch('src/sass/*.scss', ['sass']);
})